import { Platform } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View`
    margin-bottom: 7px;
    padding: 15px;
    border-radius: 4px;
    background: #fff;
 
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`;

export const BarraItem = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const Avatar = styled.Image`
    width: 50px;
    height: 50px;
    border-radius: 25px;
`;

export const IconOption = styled.View`
    width: 20px;

    border-radius: 25px;
`;


export const Info = styled.View`
    margin-left: 10px; 
    flex: 2;
`;

export const Currency = styled.View`
    margin-left: 10px;

`;

export const Name = styled.Text`
    font-weight: bold;
    font-size: 14px;
    color: #333;
`;

export const DescricaoExtrato = styled.Text`
    font-size: 15px;
    color: #999;
    margin-top: 4px;
`;

export const DataExtrato = styled.Text`
    font-size: 15px;
    color: #000;
`;