import React from 'react';
//http://spalaor.blogspot.com/2011/03/mascara-javascript-com-expressoes.html
//mascara cep
export function mascaraCep(value)
{
    value = value.replace(/\D/g,"");//Remove tudo o que não é dígito
        
    value = value.replace(/(\d{5})(\d)/,"$1-$2");//00000-000
    value = value.substr(0,9); 

    return value;
}