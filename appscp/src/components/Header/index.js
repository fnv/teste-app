import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { View, Text, Image, Alert } from 'react-native';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';


import { Container, Left, Avatar, Info, Name, Time, ViewIcon } from './styles';
import { TouchableOpacity } from 'react-native-gesture-handler';

//import Notificacao from '../../../src/screens/private/Notificacao';
//import ServicosItens from '../../../src/screens/private/ServicosItens';


/**
 source={{ uri: 'https://api.adorable.io/avatar/50/rocketseat.png' }}
 */
export default function Header() {
    const investidor = useSelector(state => state.user.profile);
    
    let razao_social = null;
    let documento = null;
 
    if(investidor){
        //pego somento o primeiro nome do investidor e faço a exibição
        let nome_apresentacao = investidor.razao_social.split(' ');
        let nome_apr = nome_apresentacao[0].toLowerCase();//convertendo todo nome em minusculo

        let str = nome_apr;
        nome_apr = str[0].toUpperCase() + str.substr(1);//convertendo a primeira letra do nome em maiusculo

        razao_social = 'Olá, '+nome_apr;
        documento = investidor.documento_label;
    }

    const navigation = useNavigation(); //faz a parte de navegação entre telas


    //console.log('*********');
    //console.log(user);
    //console.log('==============');
    //console.log(user.id);

    //const { navigate } = this.props.navigation;
    // function ServicosItens()
        //navigation.navigate('ServicosItens');  
    //}

    return (
        
        <Container>
            <Left>
                <Avatar                
                source={require('~/assets/avatar_app.png')}
                />
                
                <Info>
                    <Name>{razao_social}</Name>
                    <Time>{documento}</Time>
                </Info>
            </Left>

            <ViewIcon>
                <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <Icon name="notifications" size={20} color="#fff" />
                </TouchableOpacity>  
            </ViewIcon>

            <ViewIcon>
                <TouchableOpacity onPress={() => navigation.navigate('ServicosItens')} >
                    <Icon name="menu" size={20} color="#fff" />
                </TouchableOpacity>
            </ViewIcon>

        </Container>
      
    );      

}

