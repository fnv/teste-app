import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { useSelector } from 'react-redux';

import GeneralStatusBarColor from '~/components/GeneralStatusBarColor';

import Routes from './routes';

export default function App() {
  const signed = useSelector(state => state.auth.signed);

  //const Routes = createRouter(signed);

  return (
      <>
        <GeneralStatusBarColor backgroundColor="#fff" barStyle="dark-content"/>

        <Routes signed={signed} />
      </>
    );
}