import React, { useRef, useState } from 'react';
import { View, Text, Image, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'; 
import { signInRequest } from '~/store/modules/auth/actions';

import api from '~/services/api';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Formik, useField } from 'formik';

import FormSchema from './validation';

import { Container, Form, FormInput, SubmitButton, 
  SignLink, SignLinkText, LogoTpo, RegisterTitle, RegisterText, 
  RegisterBox1, FormInputErrors } from './styles3';

export default function IForgotMyPassword({ navigation }) {

  const [loading, setLoading] = useState(false); 
  
  async function sendSubmit(values){
        
    try {

      setLoading(true); 
      
      let email = values.ck_email;

      const response = await api.post('/password/reset', {
        email
      }); 

      if(response.data.success){
        
        Alert.alert(
          'Sucesso',
          response.data.result
        ); 

        navigation.navigate('LogIn');


      }else{

        Alert.alert(
          'Aporte - Erro',
          response.data.result
        ); 

        setLoading(false); 
      }

      
    } catch (err) {
      console.log('Erro:', err);
    }

  }

  return (
  
      <Background>
          <Container>
              <LogoTpo>
                <Image source={logo} />
              </LogoTpo>

              <RegisterTitle>
                Recuperar senha
              </RegisterTitle>

              <RegisterBox1>
                <RegisterText>Digite seu e-mail abaixo e nós lhe enviaremos instruções alterar a sua senha!</RegisterText> 
              </RegisterBox1>

            <Form>
              <Formik
                initialValues={{
                  ck_email: '',
                }}
                onSubmit={values => {
                  
                  sendSubmit(values); //chamando a função para enviar os dados do form pela api

                }}
                validationSchema={FormSchema}
              >                
                  {({values, 
                  handleChange, 
                  handleSubmit, 
                  errors,
                  touched,
                  setFieldTouched,
                  setFieldValue
                }) => (

              <Form>
                  <FormInput 
                      icon="mail"
                      keyboardType="email-address"
                      autoCorrect={false}
                      autoCapitalize="none"
                      placeholder="seuemail@dominio.com"
                      onSubmitEditing={handleSubmit} 
                      returnKeyType={ 'done' }
                      //value={login}  
                      //onChangeText={setLogin}  
                      
                      value={values.ck_email}   
                        
                      onChangeText={handleChange('ck_email')}
                        
                      onBlur={() => setFieldTouched('ck_email',true)} 
                  />   
                  {errors.ck_email && touched.ck_email && <FormInputErrors>{errors.ck_email}</FormInputErrors>}
                
                  
                <SubmitButton loading={loading} onPress={handleSubmit}>Enviar</SubmitButton>

              </Form>

            )}
            </Formik>


            </Form>

          </Container>            

      </Background>
  );
}