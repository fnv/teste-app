import * as Yup from 'yup';

export default Yup.object().shape({

  documento: Yup.string()    
    .required('Documento obrigatório')
    .min(18, 'Mínimo 18 caracteres')
    .max(18, 'Máximo 18 caracteres'),
  razaoSocial: Yup.string()    
    .required('Razão social obrigatório'),
  email: Yup.string()
    .email('E-mail inválido')
    .required('E-mail obrigatório'),
  celular: Yup.string()    
    .required('Celular obrigatório'),  
  
});

/*
name2: Yup.string()    
    .min(4, 'Mínimo 4 caracteres')
    .max(20, 'Máximo 20 caracteres'),
  name: Yup.string()    
    .min(4, 'Mínimo 4 caracteres')
    .max(20, 'Máximo 20 caracteres'),
  email: Yup.string()
    .email('E-mail inválido')
    .required('E-mail obrigatório'),
  password: Yup.string()
    .min(4, 'Mínimo 4 caracteres')
    .required('Obrigatório'),
  passwordConfirm: Yup.string()
    .min(4, 'Mínimo 4 caracteres')
    .oneOf([Yup.ref('password'), null], 'As senhas não correspondem')
    .required('Obrigatório'),
*/