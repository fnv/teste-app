import React, { useRef, useState } from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {Formik} from 'formik';
import TextInput from './TextInput';
import validation from './validation';


import api from '~/services/api';

import { mascaraCPFCNPJ } from '~/components/MaskInput/mascaraCPFCNPJ';
import { mascaraTelefone } from '~/components/MaskInput/mascaraTelefone';

import { Container, Form, FormInput, SubmitButton,
  RegisterTitle, RegisterBox1, RegisterText } from './styles2';

function PageForm({onSubmit, initialValues, style}) {

  const razaoSocialRef = useRef();
  const celularRef = useRef();

  const [documento, setDocumento] = useState('');
  const [razaoSocial, setRazaoSocial] = useState('');  
  const [celular, setCelular] = useState('');
  
  const [loading, setLoading] = useState(false);

  //mascara de CPFCNPJ ao digitar no campo
  function handleChangeMaskCPFCNPJ(value){
    setDocumento(mascaraCPFCNPJ(value)); 
  }  

  //mascara de celular ao digitar no campo
  function handleChangeMaskCelular(value){
    setCelular(mascaraTelefone(value)); 
  }  
  
  async function handleSubmit(){
    
    try {
      setLoading(true); 

      let razao_social = razaoSocial;

      const response = await api.post('/investidor/novo/app', {
        documento, 
        razao_social,
        celular
      }); 

      if(response.data.success){
        
        setLoading(false); 
        setDocumento('');
        setRazaoSocial(''); 
        setCelular(''); 

        navigation.navigate('RegisterAddress', { investidor_id: response.data.investidor_id });

      }else{

        Alert.alert(
          'Aporte - Erro',
          response.data.msg
        ); 

        setLoading(false); 
      }
      
         
    } catch (err) {
      //console.log('Erro:', err);

      Alert.alert(
        'Erro',
        err
      ); 

      setLoading(false); 
    }
    
  }



  const renderForm = ({
    values,
    setFieldValue,
    setFieldTouched,
    touched,
    errors,
    handleSubmit,
    isValid,
    isSubmitting,
  }) => {
    return (
      <Form>

          
<FormInput                       
                      keyboardType="numeric"
                      autoCorrect={false}
                      autoCapitalize="none"
                      placeholder="Digite seu CPF"
                      returnKeyType="next"  
                      onSubmitEditing={() => razaoSocialRef.current.focus()}   
                      value={documento}       
                      onChangeText={setDocumento => handleChangeMaskCPFCNPJ(setDocumento)}          
                  />
                 

        <TextInput
          onChange={setFieldValue}
          onTouch={setFieldTouched}
          placeholder="Nome"
          name="name2"
          value={values.name2}
          error={touched.name2 && errors.name2}
        />
        <TextInput
          onChange={setFieldValue}
          onTouch={setFieldTouched}
          keyboardType="email-address"
          autoCapitalize="none"
          placeholder="E-mail"
          name="email"
          value={values.email}
          error={touched.email && errors.email}
        />
        <TextInput
          onChange={setFieldValue}
          onTouch={setFieldTouched}
          placeholder="Senha"
          name="password"
          secureTextEntry={true}
          value={values.senha}
          error={touched.password && errors.password}
        />
        <TextInput
          onChange={setFieldValue}
          onTouch={setFieldTouched}
          placeholder="Confirmar senha"
          name="passwordConfirm"
          secureTextEntry={true}
          value={values.passwordConfirm}
          error={touched.passwordConfirm && errors.passwordConfirm}
        />
        <TouchableOpacity
          disabled={!isValid || isSubmitting}
          onPress={handleSubmit}
          style={StyleSheet.flatten([
            styles.submit,
            !isValid ? styles.submitDisabled : null,
          ])}>
          <Text style={styles.submitText}>Enviar</Text>
        </TouchableOpacity>
      </Form>
    );
  };

  return (
    <Formik
  initialValues={initialValues}
  onSubmit={onSubmit}
  render={renderForm}
  validationSchema={validation}
/>
  );
}

PageForm.defaultProps = {
  initialValues: {},
  onSubmit: () => null,
};

const styles = StyleSheet.create({
  container: {},
  submit: {
    height: 50,
    backgroundColor: '#f2b50c',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 6,
  },
  submitText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  submitDisabled: {
    backgroundColor: '#d1cfcf',
  },
});

export default PageForm;