import React, { useRef, useState } from 'react';
import { View, Text, Image, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'; 
import { signInRequest } from '~/store/modules/auth/actions';

import api from '~/services/api';

import { mascaraCPFCNPJ } from '~/components/MaskInput/mascaraCPFCNPJ';
import { mascaraTelefone } from '~/components/MaskInput/mascaraTelefone';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Container, Form, FormInput, SubmitButton,
  RegisterTitle, RegisterBox1, RegisterText } from './styles2';

  
export default function RegisterPF({ navigation }) {

  const razaoSocialRef = useRef();
  const celularRef = useRef();
  const emailRef = useRef();

  const [documento, setDocumento] = useState('');
  const [razaoSocial, setRazaoSocial] = useState('');  
  const [email, setEmail] = useState('');
  const [celular, setCelular] = useState('');

  
  const [loading, setLoading] = useState(false);

  //mascara de CPFCNPJ ao digitar no campo
  function handleChangeMaskCPFCNPJ(value){
    setDocumento(mascaraCPFCNPJ(value)); 
  }  

  //mascara de celular ao digitar no campo
  function handleChangeMaskCelular(value){
    setCelular(mascaraTelefone(value)); 
  }  
  
  async function handleSubmit(){
    
    try {
      setLoading(true); 

      let razao_social = razaoSocial;

      const response = await api.post('/investidor/novo/app', {
        documento, 
        razao_social,
        email,
        celular
      });  

      if(response.data.success){
        
        setLoading(false); 
        setDocumento('');
        setRazaoSocial('');
        setEmail('');  
        setCelular(''); 

        navigation.navigate('RegisterAddress', { investidor_id: response.data.investidor_id });

      }else{

        Alert.alert(
          'Aporte - Erro',
          response.data.msg
        ); 

        setLoading(false); 
      }
      
         
    } catch (err) {
      //console.log('Erro:', err);

      Alert.alert(
        'Erro',
        err
      ); 

      setLoading(false); 
    }
    
  }

  return (
  
      <Background>
          <Container>
              <Image source={logo} />

              <RegisterTitle>
                Seus dados
              </RegisterTitle>

              <RegisterBox1>
                <RegisterText>Preencha as informações abaixo.</RegisterText> 
              </RegisterBox1>

              <Form>
                  <FormInput                       
                      keyboardType="numeric"
                      autoCorrect={false}
                      autoCapitalize="none"
                      placeholder="Digite seu CPF"
                      returnKeyType="next"  
                      onSubmitEditing={() => razaoSocialRef.current.focus()}   
                      value={documento}       
                      onChangeText={setDocumento => handleChangeMaskCPFCNPJ(setDocumento)}          
                  />
  
                  <FormInput 
                      keyboardType="default"
                      placeholder="Nome completo"                     
                      returnKeyType="next"
                      onSubmitEditing={() => celularRef.current.focus()}  
                      value={razaoSocial}   
                      onChangeText={setRazaoSocial} 
                  /> 

                  <FormInput 
                      keyboardType="phone-pad"
                      placeholder="DDD e Celular"                      
                      returnKeyType="send"
                      ref={celularRef}
                      onSubmitEditing={handleSubmit} 
                      value={celular}   
                      onChangeText={setCelular => handleChangeMaskCelular(setCelular)}
                  />

                <SubmitButton loading={loading} onPress={handleSubmit}>Continuar</SubmitButton>

              </Form>


          </Container>            

      </Background>
  );
}

