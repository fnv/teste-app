import { Platform } from 'react-native';
import styled from 'styled-components/native';

import Input from '~/components/Input';
import Button from '~/components/Button';

export const Container = styled.KeyboardAvoidingView.attrs({
    enabled: Platform.OS === 'ios',
    behavior: 'padding',
})`
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0 30px;
`;

export const Form = styled.SafeAreaView`
    align-self: stretch;
    margin-top: 20px;
`;

export const FormInput = styled(Input)`
    margin-bottom: 10px;
`;

export const SubmitButton = styled(Button)`
    margin-top: 5px;
`;

export const SignLink = styled.TouchableOpacity`
    margin-top: 20px;
`;

export const SignLinkText = styled.Text`
    color: #084296;
    font-weight: bold;
    font-size: 16px;
`;

export const RegisterTitle = styled.Text`
    margin-top: 20px;
    color: #084296;
    font-weight: bold;
    font-size: 20px;
`;

export const RegisterBox1 = styled.TouchableOpacity`
    margin-top: 20px;    
    justify-content: center;
    align-items: center;
`;

export const RegisterText = styled.Text`
    color: #084296;
    font-size: 16px;
`; 

export const LogoTpo = styled.SafeAreaView`
    align-self: stretch;
    margin-top: 20px; 
    justify-content: center;
    align-items: center;
`;

export const FormInputErrors = styled.Text`
    color: red;
    font-size: 14px;
    margin-left: 30px;
    margin-bottom: 10px;
`; 