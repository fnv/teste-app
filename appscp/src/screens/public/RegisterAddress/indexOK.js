import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Image, Alert, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'; 
import { Picker } from '@react-native-community/picker';

import api from '~/services/api';
import { mascaraCep } from '~/components/MaskInput/mascaraCep';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Container, Form, FormInput, SubmitButton, LogoTpo,
  RegisterTitle, RegisterBox1, RegisterText } from './styles3';
export default function RegisterAddress({ route, navigation }) {

  const [loading, setLoading] = useState(false);

  const cepRef = useRef();
  const logradouroRef = useRef();  
  const numeroRef = useRef();
  const complementoRef = useRef();
  const bairroRef = useRef();
  const cidadeRef = useRef();
  const ufRef = useRef();

  const { investidor_id} = route.params; 
  //let investidor_id = navigation.params.investidor_id;
  const [cep, setCep] = useState('');
  const [logradouro, setLogradouro] = useState('');
  const [numero, setNumero] = useState('');
  const [complemento, setComplemento] = useState('');
  const [bairro, setBairro] = useState('');  
  const [cidade, setCidade] = useState('');
  const [uf, setUF] = useState('');

  //combobox do estado-------------------------------------------------
  const [extracts, setExtracts] = useState([]);   
  
  const [selectedValue, setSelectedValue] = useState(0);//setando o mês selecionado

  const [items, setItems] = useState([]);//itens dos meses

  const lista = [
    {name_extensive: 'Selecione', name_value: ''},
    {name_extensive: 'Acre', name_value: 'AC'},
    {name_extensive: 'Alagoas', name_value: 'AL'},
    {name_extensive: 'Amapá', name_value: 'AP'},
    {name_extensive: 'Amazonas', name_value: 'AM'}, 
    {name_extensive: 'Bahia', name_value: 'BA'},
    {name_extensive: 'Ceará', name_value: 'CE'},
    {name_extensive: 'Distrito Federal', name_value: 'DF'},
    {name_extensive: 'Espírito Santo', name_value: 'ES'},
    {name_extensive: 'Goiás', name_value: 'GO'},
    {name_extensive: 'Maranhão', name_value: 'MA'},
    {name_extensive: 'Mato Grosso', name_value: 'MT'},
    {name_extensive: 'Mato Grosso do Sul', name_value: 'MS'},
    {name_extensive: 'Minas Gerais', name_value: 'MG'},
    {name_extensive: 'Pará', name_value: 'PA'},
    {name_extensive: 'Paraíba', name_value: 'PB'},
    {name_extensive: 'Paraná', name_value: 'PR'},
    {name_extensive: 'Pernambuco', name_value: 'PE'},
    {name_extensive: 'Piauí', name_value: 'PI'},
    {name_extensive: 'Rio de Janeiro', name_value: 'RJ'},
    {name_extensive: 'Rio Grande do Norte', name_value: 'RN'},
    {name_extensive: 'Rio Grande do Sul', name_value: 'RS'},
    {name_extensive: 'Rondônia', name_value: 'RO'},
    {name_extensive: 'Roraima', name_value: 'RR'},
    {name_extensive: 'Santa Catarina', name_value: 'SC'},
    {name_extensive: 'São Paulo', name_value: 'SP'},
    {name_extensive: 'Sergipe', name_value: 'SE'},
    {name_extensive: 'Tocantins', name_value: 'TO'}
  ];

  //listando os estados para seleção--------------------------------------------------------
  async function getSelected() { 
    const body = await lista;
    //const body = await api.get(`/meses-extrato`);//qdo vim da api, tem que ter "data" body.data.map
    //console.log(body);//vindo dados do lista
    //const body = await lista.json();
    setItems(body.map(({ name_extensive,name_value }) => ({ label: name_extensive, value: name_value })));
  }
  //--------------------------------------------------------------

  //mascara de cep ao digitar no campo
  async function handleChangeMaskCep(value){
    setCep(mascaraCep(value)); 
    let cepSearch = value.substr(0,9); 

    if(cepSearch?.length !== 9){

      setLogradouro(''); 
      setBairro(''); 
      setCidade(''); 
      setUF('');

      return;

    }

    try {

      const response = await api.get(`https://viacep.com.br/ws/${cepSearch}/json/`); 

      if(!response.erro){
       
        setLogradouro(response.data.logradouro); 
        setBairro(response.data.bairro); 
        setCidade(response.data.localidade); 
        //setUF(response.data.uf); 
        setSelectedValue(response.data.uf);

      }else{

        console.log('Erro else:');
      }      
         
    } catch (err) {
      console.log('Erro:', err);
    }
  }  

  //setando a UF selecionada
  async function checkSelectedUF(value){
    setSelectedValue(value);
  }  
  
  async function handleSubmit(){
    
    try {
      setLoading(true); 

      let uf = selectedValue;
      
      const response = await api.post('/investidor/updateAddressInvestidor/app', {
        investidor_id, 
        cep,
        logradouro,
        numero,
        complemento,
        bairro,
        cidade,
        uf
      }); 

      if(response.data.success){
        
        setLoading(false); 
        setCep('');
        setLogradouro(''); 
        setNumero(''); 
        setComplemento(''); 
        setBairro(''); 
        setCidade(''); 
        setUF(''); 
        
        navigation.navigate('RegisterBank', { investidor_id: investidor_id });

      }else{

        Alert.alert(
          'Aporte - Erro',
          response.data.msg
        ); 

        setLoading(false); 
      }
      
         
    } catch (err) {
      //console.log('Erro:', err);

      Alert.alert(
        'Erro',
        err
      ); 

      setLoading(false); 
    }

  }


  //ao carregar a tela chama as funcoes-----------------------------------------------------------------
  useEffect(() =>{    

    getSelected();

  }, []);

  return (
  
      <Background>
          <Container>
            <ScrollView>
                <LogoTpo>
                  <Image source={logo} />

                  <RegisterTitle>
                    Seus dados de endereço
                  </RegisterTitle>

                  <RegisterBox1>
                    <RegisterText>Preencha as informações abaixo.</RegisterText> 
                  </RegisterBox1>
                </LogoTpo>

                <Form> 
                <FormInput                       
                        keyboardType="numeric"
                        autoCorrect={false}
                        autoCapitalize="none"
                        placeholder="Digite seu CEP"
                        returnKeyType="next"  
                        onSubmitEditing={() => logradouroRef.current.focus()}   
                        value={cep}       
                        onChangeText={setCep => handleChangeMaskCep(setCep)}          
                    />
    
                    <FormInput 
                        keyboardType="default"
                        placeholder="Logradouro"                     
                        returnKeyType="next"
                        onSubmitEditing={() => numeroRef.current.focus()}  
                        value={logradouro}   
                        onChangeText={setLogradouro} 
                    /> 

                    <FormInput 
                        keyboardType="numeric"
                        placeholder="Número"                     
                        returnKeyType="next"
                        onSubmitEditing={() => complementoRef.current.focus()}  
                        value={numero}   
                        onChangeText={setNumero} 
                    /> 

                    <FormInput 
                        keyboardType="default"
                        placeholder="Complemento"                     
                        returnKeyType="next"
                        onSubmitEditing={() => bairroRef.current.focus()}  
                        value={complemento}   
                        onChangeText={setComplemento} 
                    /> 

                    <FormInput 
                        keyboardType="default"
                        placeholder="Bairro"                     
                        returnKeyType="next"
                        onSubmitEditing={() => cidadeRef.current.focus()}  
                        value={bairro}   
                        onChangeText={setBairro} 
                    /> 

                    <FormInput 
                        keyboardType="default"
                        placeholder="Cidade"                     
                        returnKeyType="next"
                        onSubmitEditing={() => ufRef.current.focus()}  
                        value={cidade}   
                        onChangeText={setCidade} 
                    /> 

                    <Picker 
                      selectedValue={selectedValue}
                      style={{ width: "100%", marginTop: 0, fontSize: 18}}
                      itemStyle={{ height: 100, fontSize: 18 }}
                      //onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                      onValueChange={checkSelectedUF}
                      >         
                      {items && items.map((item, k) => (
                        <Picker.Item key={k} label={item.label}  value={item.value} />
                      ))} 
                    </Picker>

                  <SubmitButton loading={loading} onPress={handleSubmit}>Continuar</SubmitButton>

                </Form>

              </ScrollView>    
          </Container>               

      </Background>
  );
}

