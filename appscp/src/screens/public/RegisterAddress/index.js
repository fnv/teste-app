import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Image, Alert, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'; 
import { Picker } from '@react-native-community/picker';

import api from '~/services/api';

import { mascaraCep } from '~/components/MaskInput/mascaraCep';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Container, Form, FormInput, SubmitButton, LogoTpo,
  RegisterTitle, RegisterBox1, RegisterText, FormInputErrors } from './styles3';

import { Formik, useField } from 'formik';

import FormSchema from './validation';

export default function RegisterAddress({ route, navigation }) {

  const [loading, setLoading] = useState(false); 

  const cepRef = useRef();
  const logradouroRef = useRef();  
  const numeroRef = useRef();
  const complementoRef = useRef();
  const bairroRef = useRef();
  const cidadeRef = useRef();
  const ufRef = useRef();

  const { investidor_id} = route.params; 
  //let investidor_id = navigation.params.investidor_id;
  const [Scep, setCep] = useState('');
  const [Slogradouro, setLogradouro] = useState('');
  const [Snumero, setNumero] = useState('');
  const [Scomplemento, setComplemento] = useState('');
  const [Sbairro, setBairro] = useState('');  
  const [Scidade, setCidade] = useState('');
  const [uf, setUF] = useState('');



  //combobox do estado-------------------------------------------------
  const [extracts, setExtracts] = useState([]);   
  
  const [selectedValue, setSelectedValue] = useState(0);//setando o mês selecionado

  const [items, setItems] = useState([]);//itens dos meses

  const lista = [
    {name_extensive: 'Selecione', name_value: ''},
    {name_extensive: 'Acre', name_value: 'AC'},
    {name_extensive: 'Alagoas', name_value: 'AL'},
    {name_extensive: 'Amapá', name_value: 'AP'},
    {name_extensive: 'Amazonas', name_value: 'AM'}, 
    {name_extensive: 'Bahia', name_value: 'BA'},
    {name_extensive: 'Ceará', name_value: 'CE'},
    {name_extensive: 'Distrito Federal', name_value: 'DF'},
    {name_extensive: 'Espírito Santo', name_value: 'ES'},
    {name_extensive: 'Goiás', name_value: 'GO'},
    {name_extensive: 'Maranhão', name_value: 'MA'},
    {name_extensive: 'Mato Grosso', name_value: 'MT'},
    {name_extensive: 'Mato Grosso do Sul', name_value: 'MS'},
    {name_extensive: 'Minas Gerais', name_value: 'MG'},
    {name_extensive: 'Pará', name_value: 'PA'},
    {name_extensive: 'Paraíba', name_value: 'PB'},
    {name_extensive: 'Paraná', name_value: 'PR'},
    {name_extensive: 'Pernambuco', name_value: 'PE'},
    {name_extensive: 'Piauí', name_value: 'PI'},
    {name_extensive: 'Rio de Janeiro', name_value: 'RJ'},
    {name_extensive: 'Rio Grande do Norte', name_value: 'RN'},
    {name_extensive: 'Rio Grande do Sul', name_value: 'RS'},
    {name_extensive: 'Rondônia', name_value: 'RO'},
    {name_extensive: 'Roraima', name_value: 'RR'},
    {name_extensive: 'Santa Catarina', name_value: 'SC'},
    {name_extensive: 'São Paulo', name_value: 'SP'},
    {name_extensive: 'Sergipe', name_value: 'SE'},
    {name_extensive: 'Tocantins', name_value: 'TO'}
  ];

  //listando os estados para seleção--------------------------------------------------------
  async function getSelected() { 
    const body = await lista;
    //const body = await api.get(`/meses-extrato`);//qdo vim da api, tem que ter "data" body.data.map
    //console.log(body);//vindo dados do lista
    //const body = await lista.json();
    setItems(body.map(({ name_extensive,name_value }) => ({ label: name_extensive, value: name_value })));
  }
  //--------------------------------------------------------------


  //async mascara de cep ao digitar no campo
  function handleChangeMaskCep(value,setFieldValue){

    let cepForm = mascaraCep(value); 

    loadingChangeMaskCep(cepForm,setFieldValue);

    return cepForm;
    
  }   

  async function loadingChangeMaskCep(value,setFieldValue){

    setCep(mascaraCep(value)); 
    //let cepSearch = value.substr(0,9); 
    let cepSearch = value.replace(/[^0-9]/g, ''); 

    if(cepSearch?.length !== 8){

      setFieldValue('logradouro', '');
      setFieldValue('numero', '');
      setFieldValue('complemento', '');
      setFieldValue('bairro', ''); 
      setFieldValue('cidade', ''); 
      setFieldValue('ck_uf', ''); 

      return;

    }

    try {

      const response = await api.get(`https://viacep.com.br/ws/${cepSearch}/json/`); 

      if(!response.erro){
      
        setFieldValue('logradouro',response.data.logradouro);
        setFieldValue('numero', '');
        setFieldValue('complemento', '');
        setFieldValue('bairro',response.data.bairro); 
        setFieldValue('cidade',response.data.localidade); 
        setFieldValue('ck_uf',response.data.uf); 

      }else{

        console.log('Erro else:');
      }      
         
    } catch (err) {
      console.log('Erro:', err);
    }
    
  }   

  //setando a UF selecionada
  async function checkSelectedUF(value){
    setSelectedValue(value);
  }  
  
  async function sendSubmit(values){
    
    try {
      setLoading(true); 

      //let cepLimpo = Scep.replace("-", ""); 

      let cep = values.cep.replace("-", ""); 
      let logradouro = values.logradouro;
      let numero = values.numero;  
      let complemento = values.complemento; 
      let bairro = values.bairro; 
      let cidade = values.cidade; 
      let uf = values.ck_uf; 

      const response = await api.post('/investidor/updateAddressInvestidor/app', {
        investidor_id, 
        cep,
        logradouro,
        numero,
        complemento,
        bairro,
        cidade,
        uf
      }); 

      if(response.data.success){
        
        setLoading(false); 
        setCep('');
        setLogradouro(''); 
        setNumero(''); 
        setComplemento(''); 
        setBairro(''); 
        setCidade(''); 
        setUF(''); 
        
        navigation.navigate('RegisterBank', { investidor_id: investidor_id });

      }else{

        Alert.alert(
          'Aporte - Erro',
          response.data.msg
        ); 

        setLoading(false); 
      }
      
         
    } catch (err) {
      //console.log('Erro:', err);

      Alert.alert(
        'Erro',
        err
      ); 

      setLoading(false); 
    }

  }


  //ao carregar a tela chama as funcoes-----------------------------------------------------------------
  useEffect(() =>{   

    getSelected();


  }, []);

  return (
  
      <Background>
          <Container>
            <ScrollView>
                <LogoTpo>
                  <Image source={logo} />

                  <RegisterTitle>
                    Seus dados de endereço
                  </RegisterTitle>

                  <RegisterBox1>
                    <RegisterText>Preencha as informações abaixo.</RegisterText> 
                  </RegisterBox1>
                </LogoTpo>

            <Form>
              <Formik
                initialValues={{
                    cep: '',
                    logradouro: '',
                    numero: '',
                    complemento: '',
                    bairro: '',
                    cidade: '',
                    ck_uf: '',
                }}
                onSubmit={values => {

                  sendSubmit(values); //chamando a função para enviar os dados do form pela api

                }}
                validationSchema={FormSchema}
              >                
                  {({values, 
                  handleChange, 
                  handleSubmit, 
                  errors,
                  touched,
                  setFieldTouched,
                  setFieldValue
                }) => (

                <Form> 
                    <FormInput                       
                        keyboardType="numeric"
                        autoCorrect={false}
                        maxLength={14} 
                        autoCapitalize="none"
                        placeholder="Digite seu CEP"
                        returnKeyType="next"  
                        onSubmitEditing={() => logradouroRef.current.focus()}   
                        value={values.cep}

                        onChangeText={text => {
                            /*
                            chamo a funcao "handleChangeMaskCep" que traz os dados do endereço em json
                            - mas com setar retorno do json no  values.logradouro para preencher o campo
                            */
                            let maskedText = handleChangeMaskCep(text,setFieldValue) 
                            setFieldValue("cep", maskedText)
                        }} 

                        onBlur={() => setFieldTouched('cep',true)}    
                    />
                    {errors.cep && touched.cep && <FormInputErrors>{errors.cep}</FormInputErrors>}

    
                    <FormInput 
                        keyboardType="default"
                        placeholder="Logradouro"                     
                        returnKeyType="next"
                        onSubmitEditing={() => numeroRef.current.focus()}  
                        value={values.logradouro}                    

                        onChangeText={handleChange('logradouro')}
                        
                        onBlur={() => setFieldTouched('logradouro',true)}  
                    /> 
                    {errors.logradouro && touched.logradouro && <FormInputErrors>{errors.logradouro}</FormInputErrors>}

                    <FormInput 
                        keyboardType="numeric"
                        placeholder="Número"                     
                        returnKeyType="next"
                        onSubmitEditing={() => complementoRef.current.focus()}  
                        value={values.numero}   
                        
                        onChangeText={handleChange('numero')}
                        
                        onBlur={() => setFieldTouched('numero',true)}  
                    /> 
     
                    <FormInput 
                        keyboardType="default"
                        placeholder="Complemento"                     
                        returnKeyType="next"
                        onSubmitEditing={() => bairroRef.current.focus()}  
                        value={values.complemento}   
                        
                        onChangeText={handleChange('complemento')}
                        
                        onBlur={() => setFieldTouched('complemento',true)} 
                    /> 
                    {errors.complemento && touched.complemento && <FormInputErrors>{errors.complemento}</FormInputErrors>}


                    <FormInput 
                        keyboardType="default"
                        placeholder="Bairro"                     
                        returnKeyType="next"
                        onSubmitEditing={() => cidadeRef.current.focus()}  
                        value={values.bairro}   
                        
                        onChangeText={handleChange('bairro')}
                        
                        onBlur={() => setFieldTouched('bairro',true)} 
                    /> 
                    {errors.bairro && touched.bairro && <FormInputErrors>{errors.bairro}</FormInputErrors>}


                    <FormInput 
                        keyboardType="default"
                        placeholder="Cidade"                     
                        returnKeyType="next"
                        onSubmitEditing={() => ufRef.current.focus()}  
                        value={values.cidade}   
                        
                        onChangeText={handleChange('cidade')}
                        
                        onBlur={() => setFieldTouched('cidade',true)} 
                    /> 
                    {errors.cidade && touched.cidade && <FormInputErrors>{errors.cidade}</FormInputErrors>}


                    <Picker 
                      //selectedValue={selectedValue} //sem formik
                      selectedValue={values.ck_uf} //com formik

                      style={{ width: "100%", marginTop: 0, fontSize: 18}}
                      itemStyle={{ height: 100, fontSize: 18 }}
                      
                      //onValueChange={checkSelectedUF} //sem formik
                      onValueChange={itemValue => setFieldValue('ck_uf', itemValue)} //com formik

                      >         
                      {items && items.map((item, k) => (
                        <Picker.Item key={k} label={item.label}  value={item.value} />
                      ))} 
                    </Picker>
                    {errors.ck_uf && touched.ck_uf && <FormInputErrors>{errors.ck_uf}</FormInputErrors>}

                  <SubmitButton loading={loading} onPress={handleSubmit}>Continuar</SubmitButton>

                  </Form>
                       
                  )}
              </Formik>

              
            </Form>

              </ScrollView>    
          </Container>               

      </Background>
  );
}

