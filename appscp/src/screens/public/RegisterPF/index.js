import React, { useRef, useState } from 'react';
import { View, Text, Image, Alert, ScrollView } from 'react-native';

import api from '~/services/api';

import { mascaraCPFCNPJ } from '~/components/MaskInput/mascaraCPFCNPJ';
import { mascaraTelefone } from '~/components/MaskInput/mascaraTelefone';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Container, Form, FormInput, SubmitButton, LogoTpo,
  RegisterTitle, RegisterBox1, RegisterText, FormInputErrors } from './styles2';

import { Formik } from 'formik';

import FormSchema from './validation';
  
export default function RegisterPF({ navigation }) {

  //const documentoRef = useRef(null);
  const razaoSocialRef = useRef();
  const celularRef = useRef(); 
  const emailRef = useRef(); 

  const [documento, setDocumento] = useState('');
  const [razaoSocial, setRazaoSocial] = useState('');  
  const [celular, setCelular] = useState('');
  const [email, setEmail] = useState('');
  
  const [loading, setLoading] = useState(false);

  //mascara de CPFCNPJ ao digitar no campo
  function handleChangeMaskCPFCNPJ(value){
    //setDocumento(mascaraCPFCNPJ(value)); 

    return mascaraCPFCNPJ(value); 
  }  

  //mascara de celular ao digitar no campo
  function handleChangeMaskCelular(value){
    //setCelular(mascaraTelefone(value)); 

    return mascaraTelefone(value);
  }  

  //enviar os dados do form pela api
  async function sendSubmit(values){
    
    try {
      setLoading(true); 

      let documento = values.documento;  
      let razao_social = values.razaoSocial; 
      let email = values.email; 
      let celular = values.celular; 

      const response = await api.post('/investidor/novo/app', {
        documento, 
        razao_social,
        email,
        celular
      });  

      if(response.data.success){
        
        setLoading(false); 
        setDocumento('');
        setRazaoSocial('');
        setEmail('');  
        setCelular(''); 

        navigation.navigate('RegisterAddress', { investidor_id: response.data.investidor_id });

      }else{

        Alert.alert(
          'Aporte - Erro',
          response.data.msg
        ); 

        setLoading(false); 
      }
      
         
    } catch (err) {
      //console.log('Erro:', err);

      Alert.alert(
        'Erro',
        err
      ); 

      setLoading(false); 
    }
   
  }
  
  return (
  
      <Background>
          <Container>
            <ScrollView>
                <LogoTpo>
                  <Image source={logo} />

                  <RegisterTitle>
                    Seus dados
                  </RegisterTitle>

                  <RegisterBox1>
                    <RegisterText>Preencha as informações abaixo.</RegisterText> 
                  </RegisterBox1>
                </LogoTpo>

              <Form>
              <Formik
                initialValues={{
                    documento: '',
                    razaoSocial: '',
                    email: '',
                    celular: '',
                }}
                onSubmit={values => {
                    
                    setDocumento(values.documento);
                    setRazaoSocial(values.razaoSocial);
                    setEmail(values.email);  
                    setCelular(values.celular); 
                   
                    sendSubmit(values); //chamando a função para enviar os dados do form pela api
                }}
                validationSchema={FormSchema}
              >                
                  {({values, 
                  handleChange, 
                  handleSubmit, 
                  errors,
                  touched,
                  setFieldTouched,
                  setFieldValue
                }) => (
                        <Form>
                           <FormInput 
                           keyboardType="numeric"
                           autoCorrect={false}
                           autoCapitalize="none"
                           placeholder="Digite seu CPF"
                           maxLength={14} 
                           onSubmitEditing={() => razaoSocialRef.current.focus()} 
                           returnKeyType="done"

                           value={values.documento}

                           onChangeText={text => {
                                let maskedText = handleChangeMaskCPFCNPJ(text)
                                setFieldValue("documento", maskedText)
                           }} 

                           onBlur={() => setFieldTouched('documento',true)}                      
                        />
                        {errors.documento && touched.documento && <FormInputErrors>{errors.documento}</FormInputErrors>}

                        <FormInput 
                            keyboardType="default"
                            placeholder="Nome completo"  
                            onSubmitEditing={() => emailRef.current.focus()}  
                            value={values.razaoSocial}   
                            returnKeyType="next"
                            ref={razaoSocialRef}
                            
                            onChangeText={handleChange('razaoSocial')}

                            onBlur={() => setFieldTouched('razaoSocial',true)} 
                        /> 
                        {errors.razaoSocial && touched.razaoSocial && <FormInputErrors>{errors.razaoSocial}</FormInputErrors>}

                        <FormInput 
                            keyboardType="default"
                            placeholder="Seu e-mail"  
                            onSubmitEditing={() => celularRef.current.focus()}  
                            value={values.email}  
                            returnKeyType="next"
                            ref={emailRef}
                            
                            onChangeText={handleChange('email')}

                            onBlur={() => setFieldTouched('email',true)} 
                        /> 
                        {errors.email && touched.email && <FormInputErrors>{errors.email}</FormInputErrors>}
 
                        <FormInput 
                            keyboardType="phone-pad"
                            placeholder="DDD e Celular"  
                            returnKeyType="send"
                            maxLength={15} 
                            ref={celularRef}
                            onSubmitEditing={handleSubmit} 
                            value={values.celular}   

                            onChangeText={text => {
                                let maskedText = handleChangeMaskCelular(text)
                                setFieldValue("celular", maskedText)
                           }} 

                            onBlur={() => setFieldTouched('celular',true)} 
                        />
                        {errors.celular && touched.celular && <FormInputErrors>{errors.celular}</FormInputErrors>}

                        <SubmitButton loading={loading} onPress={handleSubmit}>Continuar</SubmitButton>
                       
                       </Form>
                       
                  )}
              </Formik>

              
             </Form>
              
             </ScrollView>

          </Container>            

      </Background>
  );
}

