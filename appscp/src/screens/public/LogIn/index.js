import React, { useRef, useState } from 'react';
import { View, Text, Image, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'; 
import { signInRequest } from '~/store/modules/auth/actions';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Formik, useField } from 'formik';

import FormSchema from './validation';

import { Container, Form, FormInput, SubmitButton, SignLink, SignLinkText, LogoTpo, FormInputErrors } from './styles';

export default function LogIn({ navigation }) {

  const dispatch = useDispatch();
  const passwordRef = useRef();

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const loading = useSelector(state => state.auth.loading);
  /*
  function handleSubmit(){
    console.log('senha: '+login);
    console.log('senha: '+password);
    //Alert.alert('enviar dados do login');
    dispatch(signInRequest(login, password));
    //const signed = useSelector(state => state.auth.signed);//checa se o usuario está logado

    //console.log(signed);
  }
*/
   
  async function sendSubmit(values){
        
    try {
      
      let login = values.ck_login;
      let password = values.ck_password;

      dispatch(signInRequest(login, password));
      
    } catch (err) {
      console.log('Erro:', err);
    }

  }

  return (
  
      <Background>
          <Container>
              <LogoTpo>
                <Image source={logo} />
              </LogoTpo>

            <Form>
              <Formik
                initialValues={{
                  ck_login: '',
                  ck_password: '',
                }}
                onSubmit={values => {
                  
                  sendSubmit(values); //chamando a função para enviar os dados do form pela api

                }}
                validationSchema={FormSchema}
              >                
                  {({values, 
                  handleChange, 
                  handleSubmit, 
                  errors,
                  touched,
                  setFieldTouched,
                  setFieldValue
                }) => (

              <Form>
                  <FormInput 
                      icon="account-circle"
                      keyboardType="numeric"
                      autoCorrect={false}
                      autoCapitalize="none"
                      placeholder="Digite seu CPF"
                      onSubmitEditing={() => passwordRef.current.focus()}   
                      returnKeyType={ 'done' }
                      //value={login}  
                      //onChangeText={setLogin}  
                      
                      value={values.ck_login}   
                        
                      onChangeText={handleChange('ck_login')}
                        
                      onBlur={() => setFieldTouched('ck_login',true)} 
                  />   
                  {errors.ck_login && touched.ck_login && <FormInputErrors>{errors.ck_login}</FormInputErrors>}
                
                  <FormInput 
                      icon="lock-outline"
                      keyboardType="default"
                      secureTextEntry
                      placeholder="Digite sua senha"                     
                      returnKeyType="send"
                      ref={passwordRef}
                      onSubmitEditing={handleSubmit} 
                      //value={password}  
                      //onChangeText={setPassword}        
                      
                      value={values.ck_password}   
                        
                      onChangeText={handleChange('ck_password')}
                        
                      onBlur={() => setFieldTouched('ck_password',true)} 
                  />
                  {errors.ck_password && touched.ck_password && <FormInputErrors>{errors.ck_password}</FormInputErrors>}
                
                <SubmitButton loading={loading} onPress={handleSubmit}>Entrar</SubmitButton>

              </Form>

            )}
            </Formik>


            </Form>

              <SignLink onPress={() => navigation.navigate('Register')}>
                <SignLinkText>Criar conta</SignLinkText>                
              </SignLink>

              <SignLink onPress={() => navigation.navigate('IForgotMyPassword')}>
                <SignLinkText>Esqueci minha senha</SignLinkText>                
              </SignLink>

              

          </Container>            

      </Background>
  );
}