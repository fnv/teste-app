import React, { useRef, useState } from 'react';
import { View, Text, Image, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'; 
import { signInRequest } from '~/store/modules/auth/actions';


import logo from '~/assets/logo.png';
import Background from '~/components/Background';


import { Container, Form, FormInput, SubmitButton, SignLink, SignLinkText, LogoTpo } from './styles';

export default function Login({ navigation }) {

  const dispatch = useDispatch();
  const passwordRef = useRef();

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const loading = useSelector(state => state.auth.loading);
  
  function handleSubmit(){
    console.log('senha: '+login);
    console.log('senha: '+password);
    //Alert.alert('enviar dados do login');
    dispatch(signInRequest(login, password));
    //const signed = useSelector(state => state.auth.signed);//checa se o usuario está logado

    //console.log(signed);
  }

  return (
  
      <Background>
          <Container>
              <LogoTpo>
                <Image source={logo} />
              </LogoTpo>

              <Form>
                  <FormInput 
                      icon="account-circle"
                      keyboardType="numeric"
                      autoCorrect={false}
                      autoCapitalize="none"
                      placeholder="Digite seu CPF"
                      onSubmitEditing={() => passwordRef.current.focus()}   
                      value={login}   
                      returnKeyType={ 'done' }
                      onChangeText={setLogin}              
                  />   
  
                  <FormInput 
                      icon="lock-outline"
                      keyboardType="default"
                      secureTextEntry
                      placeholder="Digite sua senha"                     
                      returnKeyType="send"
                      ref={passwordRef}
                      onSubmitEditing={handleSubmit} 
                      value={password}  
                      onChangeText={setPassword} 
                      
                  />

                <SubmitButton loading={loading} onPress={handleSubmit}>Entrar</SubmitButton>

              </Form>

              <SignLink onPress={() => navigation.navigate('Register')}>
                <SignLinkText>Criar conta</SignLinkText>                
              </SignLink>

              <SignLink onPress={() => navigation.navigate('IForgotMyPassword')}>
                <SignLinkText>Esqueci minha senha</SignLinkText>                
              </SignLink>

              

          </Container>            

      </Background>
  );
}