import { Platform } from 'react-native';
import styled from 'styled-components/native';

import Input from '~/components/Input';
import Button from '~/components/Button';

export const Container = styled.KeyboardAvoidingView.attrs({
    enabled: Platform.OS === 'ios',
    behavior: 'padding',
})`
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0 30px;
`;

export const Form = styled.SafeAreaView`
    align-self: stretch;
    margin-top: 20px; 
`;

export const FormInput = styled(Input)`
    margin-bottom: 10px;
    color: #000;
`;

export const SubmitButton = styled(Button)`
    margin-top: 5px;
`;

export const SignLink = styled.TouchableOpacity`
    margin-top: 20px;
`;


export const SignLinkText = styled.Text`
    color: #084296;
    font-weight: bold;
    font-size: 16px;
`;

export const LogoTpo = styled.SafeAreaView`
    align-self: stretch;
    margin-top: 60px; 
    justify-content: center;
    align-items: center;
`;

export const FormInputErrors = styled.Text`
    color: red;
    font-size: 14px;
    margin-left: 30px;
    margin-bottom: 10px;
`; 
