import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Image, Alert, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'; 
import { Picker } from '@react-native-community/picker';

import api from '~/services/api';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Container, Form, FormInput, SubmitButton, LogoTpo,
  RegisterTitle, RegisterBox1, RegisterText, FormInputErrors } from './styles';

import { Formik, useField } from 'formik';

import FormSchema from './validation';

export default function RegisterBank({ route, navigation }) {

  const pixRef = useRef();
  const [Spix, setPix] = useState('');

  const [loading, setLoading] = useState(false);
  
  const contaRef = useRef();

  const { investidor_id} = route.params; 
  //let investidor_id = navigation.params.investidor_id;
  //const [aplicacao_automatica, setAplicacaoAutomatica] = useState('');
  //const [banco, setBanco] = useState('');
  //const [tipo_conta, setTipoConta] = useState('');
  const [agencia, setAgencia] = useState('');
  const [conta, setConta] = useState(''); 


  //combobox do bancos-------------------------------------------------
  const [extracts, setExtracts] = useState([]);   
  
  const [selectedValue, setSelectedValue] = useState('');//setando o item selecionado

  const [items, setItems] = useState([]);//itens do select
  //combobox do reinvestidor----------------------------------
  const [extractsR, setExtractsR] = useState([]);   
  
  const [selectedValueR, setSelectedValueR] = useState('');//setando o item selecionado

  const [itemsR, setItemsR] = useState([]);//itens do select

  //combobox do tipo canta----------------------------------
  const [extractsTA, setExtractsTA] = useState([]);   
  
  const [selectedValueTA, setSelectedValueTA] = useState('');//setando o item selecionado

  const [itemsTA, setItemsTA] = useState([]);//itens do select

  const [selectedValuePix, setSelectedValuePix] = useState('');//setando o item selecionado

  const [itemsPix, setItemsPix] = useState([]);//itens do select
  //-----------------------------------------------------------

  const lista2 = [
    {name_extensive: 'Deseja reinvestir a sua rentabilidade mensal?', name_value: ''},
    {name_extensive: 'Sim', name_value: 'S'},
    {name_extensive: 'Não', name_value: 'N'}
  ];
  //------------------------------------------------------------

  const lista3 = [
    {name_extensive: 'Tipo de Conta', name_value: ''},
    {name_extensive: 'Conta Corrente', name_value: 'CC'},
    {name_extensive: 'Conta Poupança', name_value: 'CP'}
  ];

  //------------------------------------------------------------

    const lista4 = [
      {name_extensive: 'Tipo do Pix', name_value: ''},
      {name_extensive: 'Pix - celular', name_value: 'celular'},
      {name_extensive: 'Pix - E-mail', name_value: 'email'},
      {name_extensive: 'Pix - CPF', name_value: 'cpf'},
      {name_extensive: 'Pix - CNPJ', name_value: 'cnpj'}
    ];

  //listando os bancos para seleção------------------------------
  async function getSelected() { 
    //const body = await lista;
    const body = await api.get(`/lista-bancos`);//qdo vim da api, tem que ter "data" body.data.map
    //console.log(body);//vindo dados do lista
    //const body = await lista.json();
    setItems(body.data.map(({ name_extensive,name_value }) => ({ label: name_extensive, value: name_value })));
  }
  //--------------------------------------------------------------

  //listando os bancos para seleção-------------------------------
  async function getSelectedReinvestir() { 
    const body = await lista2;
    //const body = await api.get(`/lista-bancos`);//qdo vim da api, tem que ter "data" body.data.map
    //console.log(body);//vindo dados do lista
    //const body = await lista.json();
    setItemsR(body.map(({ name_extensive,name_value }) => ({ label: name_extensive, value: name_value })));
  }
  //--------------------------------------------------------------

  //listando os bancos para seleção-------------------------------
  async function getSelectedTipoConta() { 
    const body = await lista3;
    //const body = await api.get(`/lista-bancos`);//qdo vim da api, tem que ter "data" body.data.map
    //console.log(body);//vindo dados do lista
    //const body = await lista.json();
    setItemsTA(body.map(({ name_extensive,name_value }) => ({ label: name_extensive, value: name_value })));
  }
  //--------------------------------------------------------------

  //listando os tipos de pix para seleção-------------------------------
  async function getSelectedTipoPix() { 
    const body = await lista4;
    //const body = await api.get(`/lista-bancos`);//qdo vim da api, tem que ter "data" body.data.map
    //console.log(body);//vindo dados do lista
    //const body = await lista.json();
    setItemsPix(body.map(({ name_extensive,name_value }) => ({ label: name_extensive, value: name_value })));
  }
  //--------------------------------------------------------------


  //setando o banco selecionado
  async function checkSelectedBank(value){
    setSelectedValue(value);
  } 

  //setando o reinvestimento selecionado
  async function checkSelectedReinvestment(value){
    setSelectedValueR(value);
  }  

  //setando o tipo conta selecionado
  async function checkSelectedTypeAccount(value){
     setSelectedValueTA(value); 
  } 

  //setando o tipo pix
  async function checkSelectedTypePix(value){
    setSelectedValuePix(value); 
 } 
  
  
  async function sendSubmit(values){

    try {
      setLoading(true); 

      let banco = values.ck_banco;
      let aplicacao_automatica = values.ck_aplicacao;
      let tipo_conta = values.ck_tp_conta;
      let agencia = values.agencia;
      let conta = values.conta;
      let tipo_pix = selectedValuePix;
      let pix = Spix;

     

      const response = await api.post('/investidor/storeInvestidorBanco/app', {
        investidor_id, 
        banco,
        agencia,
        conta,
        tipo_conta,
        aplicacao_automatica,
        tipo_pix,
        pix
      });         
     
      if(response.data.success){
        
        setLoading(false);  
        setSelectedValue(0); 
        setSelectedValueTA(''); 
        setAgencia('');   
        setConta('');
        setSelectedValueR(''); 
        setSelectedValuePix(''); 
        
        Alert.alert(
          'Sucesso',
          'Seu cadastrato foi realizado com sucesso.'
        ); 
        navigation.navigate('LogIn');

      }else{

        Alert.alert(
          'Aporte - Erro',
          response.data.msg
        ); 

        setLoading(false); 
      }
     
         
    } catch (err) {
      //////console.log('Erro:', err);

      Alert.alert(
        'Erro',
        err
      ); 

      setLoading(false); 
    }

  }


  //ao carregar a tela chama as funcoes-----------------------------------------------------------------
  useEffect(() =>{   

    getSelected();
    getSelectedReinvestir();
    getSelectedTipoConta();
    getSelectedTipoPix();

  }, []);

  return (
  
      <Background>
          <Container>
            <ScrollView>
                <LogoTpo>
                  <Image source={logo} />

                  <RegisterTitle>
                    Seus dados bancários
                  </RegisterTitle>

                  <RegisterBox1>
                    <RegisterText>Preencha as informações abaixo.</RegisterText> 
                  </RegisterBox1>
                </LogoTpo>

            <Form>
              <Formik
                initialValues={{
                  ck_banco: '',
                  ck_tp_conta: '',
                  agencia: '',
                  conta: '',
                  ck_aplicacao: '',
                }}
                onSubmit={values => {
                    
                  /*setCep(values.cep);
                  setLogradouro(values.logradouro);
                  setNumero(values.numero);  
                  setComplemento(values.complemento); 
                  setBairro(values.bairro); 
                  setCidade(values.cidade); 
                  setUF(selectedValue); */
                   
                  sendSubmit(values); //chamando a função para enviar os dados do form pela api
                }}
                validationSchema={FormSchema}
              >                
                  {({values, 
                  handleChange, 
                  handleSubmit, 
                  errors,
                  touched,
                  setFieldTouched,
                  setFieldValue
                }) => (

                <Form> 

                  <Picker 
                    //selectedValue={selectedValue} //sem formik
                    selectedValue={values.ck_banco} //com formik

                    style={{ width: "100%", marginTop: 0, fontSize: 18}}
                    itemStyle={{ height: 100, fontSize: 18 }}
               
                    //onValueChange={checkSelectedBank} //sem formik
                    onValueChange={itemValue => setFieldValue('ck_banco', itemValue)} //com formik
                    > 
                    <Picker.Item key='0' label='Selecione o banco' value='' />     
                    {items && items.map((item, k) => (
                      <Picker.Item key={k} label={item.label} value={item.value} />
                    ))} 
                  </Picker>
                  {errors.ck_banco && touched.ck_banco && <FormInputErrors>{errors.ck_banco}</FormInputErrors>}

                  <Picker 
                    //selectedValue={selectedValueTA} //sem formik
                    selectedValue={values.ck_tp_conta} //com formik

                    style={{ width: "100%", marginTop: 0, fontSize: 18}}
                    itemStyle={{ height: 100, fontSize: 18 }}
                   
                    //onValueChange={checkSelectedTypeAccount} //sem formik
                    onValueChange={itemValue => setFieldValue('ck_tp_conta', itemValue)} //com formik
                    >      
                    {itemsTA && itemsTA.map((item, k) => (
                      <Picker.Item key={k} label={item.label}  value={item.value} />
                    ))} 
                  </Picker>
                  {errors.ck_tp_conta && touched.ck_tp_conta && <FormInputErrors>{errors.ck_tp_conta}</FormInputErrors>}
                  
                    
                    <FormInput 
                        keyboardType="numeric"
                        placeholder="agencia"                     
                        returnKeyType="next"
                        value={values.agencia}   
                        
                        onChangeText={handleChange('agencia')}
                        
                        onBlur={() => setFieldTouched('agencia',true)}  
                    /> 
                    {errors.agencia && touched.agencia && <FormInputErrors>{errors.agencia}</FormInputErrors>}

                    <FormInput 
                        keyboardType="default"
                        placeholder="conta"                     
                        returnKeyType="next"
                        value={values.conta}   
                        
                        onChangeText={handleChange('conta')}
                        
                        onBlur={() => setFieldTouched('conta',true)} 
                    /> 
                    {errors.conta && touched.conta && <FormInputErrors>{errors.conta}</FormInputErrors>}


                    <Picker 
                    //selectedValue={selectedValueR} //sem formik
                    selectedValue={values.ck_aplicacao} //com formik
                    
                    /*
                    style={{ height: 50, width: 350, marginTop: 0, fontSize: 18 }}
                    itemStyle={{ height: 100, fontSize: 18 }}
                    */
                    style={{ width: "100%", marginTop: 0, fontSize: 18}}
                    itemStyle={{ height: 100, fontSize: 18 }}
                    
                    //onValueChange={checkSelectedReinvestment} //sem formik
                    onValueChange={itemValue => setFieldValue('ck_aplicacao', itemValue)} //com formik
                    >         
                    {itemsR && itemsR.map((item, k) => (
                      <Picker.Item key={k} label={item.label}  value={item.value} />
                    ))} 
                  </Picker>  
                  {errors.ck_aplicacao && touched.ck_aplicacao && <FormInputErrors>{errors.ck_aplicacao}</FormInputErrors>}

                  <Picker 
                    selectedValue={selectedValuePix} //sem formik
                    //selectedValue={values.ck_tp_conta} //com formik

                    style={{ width: "100%", marginTop: 0, fontSize: 18}}
                    itemStyle={{ height: 100, fontSize: 18 }}
                   
                    onValueChange={checkSelectedTypePix} //sem formik
                    //onValueChange={itemValue => setFieldValue('ck_tp_conta', itemValue)} //com formik
                    >      
                    {itemsPix && itemsPix.map((item, k) => (
                      <Picker.Item key={k} label={item.label}  value={item.value} />
                    ))} 
                  </Picker>

                  <FormInput 
                      keyboardType="default"
                      placeholder="Digite a chave do Pix"                     
                      returnKeyType="send"
                      ref={pixRef}
                      onSubmitEditing={handleSubmit} 
                      value={pixRef}  
                      onChangeText={setPix} 
                      
                  />

                  <SubmitButton loading={loading} onPress={handleSubmit}>Finalizar</SubmitButton>

                  </Form>
                       
                  )}
              </Formik>

              
            </Form>

              </ScrollView>    
          </Container>               

      </Background>
  );
}

