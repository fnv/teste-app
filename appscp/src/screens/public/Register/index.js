import React, { useRef, useState } from 'react';
import { Image, TouchableOpacity } from 'react-native';

//import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';

import logo from '~/assets/logo.png';
import Background from '~/components/Background';

import { Container, RegisterTitle, ServicesRow, Services, 
  Name, RegisterText, RegisterBox1, RegisterBox2, LogoTpo, ViewServices,
  IconOption } from './styles';

export default function Register({ navigation }) {

  //var iconPessoaFisica = <Icon name="account-circle" size={30} color="#000" />;
  //var iconPessoaJuridica = <Icon name="card-travel" size={30} color="#000" />;

  return (

      <Background>
          <Container>
              <LogoTpo>
                <Image source={logo} />
              

                <RegisterTitle>
                  Bem - vindo!
                </RegisterTitle>

                <RegisterBox1>
                  <RegisterText>Estamos felizes em ter você por aqui.</RegisterText>  
                </RegisterBox1>

                <RegisterBox1>
                  <RegisterText>Vamos começar?</RegisterText>                
                </RegisterBox1>
                
                <RegisterBox2>
                  <RegisterText>Escolha o tipo de conta que quer abrir.</RegisterText>               
                </RegisterBox2>

              </LogoTpo>

              <ServicesRow>
               
                  <Services onPress={() => navigation.navigate('RegisterPF')} >
                    <ViewServices>            
                      <Icon name="person-circle" size={30} color="#084296" />                  
                      <Name>Para você</Name>
                    </ViewServices>
                  </Services>

                  <Services onPress={() => navigation.navigate('RegisterPJ')} >
                    <ViewServices>            
                      <Icon name="business" size={30} color="#084296" />                  
                      <Name>Para sua empresa</Name>
                    </ViewServices>
                  </Services>
                
              </ServicesRow>
              
          </Container>            

      </Background>
  );
}
