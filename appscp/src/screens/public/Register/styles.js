import { Platform } from 'react-native';
import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';

import Input from '~/components/Input';
import Button from '~/components/Button';

export const Container = styled.KeyboardAvoidingView.attrs({
    enabled: Platform.OS === 'ios',
    behavior: 'padding',
})`
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0 30px;
`;

export const Form = styled.SafeAreaView`
    align-self: stretch;
    margin-top: 50px;
`;

export const FormInput = styled(Input)`
    margin-bottom: 10px;
`;

export const SubmitButton = styled(Button)`
    margin-top: 5px;
`;

export const RegisterBox1 = styled.TouchableOpacity`
    margin-top: 20px;    
    /*justify-content: center;
    align-items: center;*/
`;

export const RegisterBox2 = styled.TouchableOpacity`
    margin-top: 30px;    
    justify-content: center;
    align-items: center;
`;

export const ViewServices = styled.View`
    align-items: center;
`;


export const LogoTpo = styled.SafeAreaView`
    align-self: stretch;
    margin-top: 20px; 
    justify-content: center;
    align-items: center;
`;



export const RegisterText = styled.Text`
    color: #084296;
    font-size: 16px;
`;

export const RegisterTitle = styled.Text`
    margin-top: 20px;
    color: #084296;
    font-weight: bold;
    font-size: 20px;
`;

export const ServicesRow = styled.View`
    flex-direction: row;
    align-items: center;
    padding: 1px;
`;

export const Services = styled(RectButton)`
   background: #fff;
   border-radius: 4px;
   padding: 20px;
   flex: 1;

   height: 100px;

   align-items: center;
   margin: 10px 10px 10px;
`;

export const Avatar = styled.Image`
   width: 50px;
   height: 50px;
   border-radius: 25px;
`;

export const Name = styled.Text`
   margin-top: 15px;
   font-size: 14px;
   font-weight: bold;
   color: #333;
   text-align: center;
`;

export const IconOption = styled.View`
    width: 30px;
    height: 10px;
    border-radius: 25px;
    margin-bottom: 15px;
`;
