import { Platform } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.KeyboardAvoidingView.attrs({
    enabled: Platform.OS === 'ios',
    behavior: 'padding',
})`
    flex: 1;
`;

export const Title = styled.Text`
   font-size: 20px;
   color: #000;
   font-weight: bold;
   align-self: center;
   margin-top: 10px;
   margin-bottom: 10px;
`;

export const List = styled.FlatList.attrs({
    showsVerticalScrollIndicator: false,
    contentContainerStyle: { padding: 30 },
})``;


export const BarraItem = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`;


//parte de fechamento
export const RowPicker = styled.View`
    flex-direction: row;
    align-items: center;
    padding: 1px;
`;

export const InfoPicker = styled.View`
    background: #fff;
    border-radius: 4px;
    flex: 1;
    

    align-items: center;
    margin: 10px 30px 10px;
`;
