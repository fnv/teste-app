import React, { useEffect, useState, useCallback } from 'react';
import { View, Text, Image, Alert, ScrollView } from 'react-native';
import { useSelector } from 'react-redux';

import Background from '~/components/BackgroundLogged';
import Header from '~/components/Header';
import Loading from '~/components/Loading';
import api from '~/services/api';

import { Container, Title, RowClosure, InfoClosure, TitleClosure, ValueClosure,
  RowCurrentCapital, InfoCurrentCapital, InfoCurrentCapitalCenter, TitleCurrentCapital, ValueCurrentCapital,
  DetailingCurrentCapital, DetailingCurrentCapitalRight, DetailingCurrentCapitalLeft,
  DetailingRowCurrentCapital, TitleDetailing, SubTitleDetailing, ResultSubTitleDetailing } from './styles';

export default function Dashboard({ navigation }) {

  const [loading, setLoading] = useState(false);

  const [refreshing, setRefreshing] = useState(false);//refreshing da tela

  const token = useSelector(state => state.auth.token);

  const investidor = useSelector(state => state.user.profile);

  let investidor_id = 0;
  if(investidor)
    investidor_id = investidor.id;

  //variaveis de state---------------------------------------------------------------------------------

  const [varDataUltimoFechamento, setVarDataUltimoFechamento] = useState('dd/mm/aaaa');
  const [varValorUltimoFechamento, setVarValorUltimoFechamento] = useState('R$ 0.000,00');
  const [varDataAtual, setVarDataAtual] = useState('dd/mm/aaaa');
  const [varValorAtual, setVarValorAtual] = useState('R$ 0.000,00');
  const [varMesAtual, setVarMesAtual] = useState('**/**');
  const [varResultadoValorRecebido, setVarResultadoValorRecebido] = useState('R$ 0.000,00');
  const [varRentabilidade, setVarRentabilidade] = useState('1,0000');
  const [varCapitalMesAtual, setVarCapitalMesAtual] = useState('R$ 0.000,00');
  const [varCapitalSocialAtual, setVarCapitalSocialAtual] = useState('R$ 0.000,00');

   //ao clicar no tabNavigaator da tela, a mesma é recarregada
   async function unsubscribe(){
    
    const unsubscribe = navigation.addListener('focus', () => {    
      let value = null;  
      dadosDashboard();//recarregando os dados da tela
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }  

  //listando o extrato---------------------------------------------------------------------------------
  async function dadosDashboard() {
 
    try {

        setLoading(true);

        if (token) api.defaults.headers.Authorization = `Bearer ${token}`;

            const response = await api.get(`/investidor/dashboardApp/${investidor_id}`);

        if(response.data.success){

            //******tratar o response qdo expirar o token*********

            //console.log('----------------extrato--------------------');
            console.log(response.data.results);  
            //setExtracts(response.data.extratos);       

            setVarDataUltimoFechamento(response.data.results.invest_capital_ultimo_mes_data);
            setVarValorUltimoFechamento(response.data.results.invest_capital_ultimo_mes_valor);
            setVarDataAtual(response.data.results.invest_capital_atual_mes_data);
            setVarValorAtual(response.data.results.invest_capital_atual_mes_valor);
            setVarMesAtual(response.data.results.escrita_do_mes_atual+'/'+response.data.results.ano_do_mes_atual);
            setVarResultadoValorRecebido(response.data.results.resultado_valor_recebido);
            setVarRentabilidade(response.data.results.rentabilidade);
            setVarCapitalMesAtual(response.data.results.capital_mes_atual);      
            setVarCapitalSocialAtual(response.data.results.capital_social_atual);          
            
            setLoading(false);

        }else{
          //QUANDO EXPIRA O TOKEN, É MELHOR REDIRECIONAR PARA LOGAR OU DAR REFRESH NO TOKEN?
          //console.log('ErroX1:', err);
          //console.log('ErroX1:');
          //console.log(response.data);  
          Alert.alert(
            'Aporte - Erro',
            'espirou o token'
            //response.data.msg
          ); 
  
          setLoading(false); 

        }

    } catch (err) {
        //console.log('ErroX2:', err);
        //console.log('ErroX2:');
        //console.log(response.data.extratos);  
        Alert.alert(
          'Erro catch',
          err
        ); 

        setLoading(false); 
    }
  }
 
    //ao carregar a tela chama as funcoes-----------------------------------------------------------------
    useEffect(() =>{

      unsubscribe();

      dadosDashboard();

    }, []);
   
  return (
  
      <Background>
        <Header />
          <Container>

            

                {loading &&
                  <Loading />
                }
                {!loading &&
      
                  <>

                    <ScrollView>
                    
                      <RowClosure>                
                          <InfoClosure>
                              <TitleClosure>Fechamento {varDataUltimoFechamento}</TitleClosure>
                              <ValueClosure>{varValorUltimoFechamento}</ValueClosure>
                          </InfoClosure>
                      </RowClosure>

                      <RowCurrentCapital>                
                          <InfoCurrentCapitalCenter>
                              <TitleCurrentCapital>Capital Atual {varDataAtual}</TitleCurrentCapital>
                              <ValueCurrentCapital>{varValorAtual}</ValueCurrentCapital>
                          </InfoCurrentCapitalCenter>
                      </RowCurrentCapital>

                      <RowCurrentCapital>  
                        <DetailingCurrentCapitalLeft>             
                          <InfoCurrentCapital>
                              <TitleDetailing>Resultados {varMesAtual}</TitleDetailing>
                              <SubTitleDetailing>Rentabilidade</SubTitleDetailing>
                              <ResultSubTitleDetailing>{varRentabilidade}%</ResultSubTitleDetailing>
                              <SubTitleDetailing>Result. Capital</SubTitleDetailing>
                              <ResultSubTitleDetailing>R$ {varResultadoValorRecebido}</ResultSubTitleDetailing>
                              <SubTitleDetailing>Valor Recebido</SubTitleDetailing>
                              <ResultSubTitleDetailing>R$ {varResultadoValorRecebido}</ResultSubTitleDetailing>
                          </InfoCurrentCapital>
                        </DetailingCurrentCapitalLeft> 

                        <DetailingCurrentCapitalRight>             
                          <InfoCurrentCapital>
                              <TitleDetailing>Capital Atual</TitleDetailing>
                              <SubTitleDetailing>{varMesAtual}</SubTitleDetailing>
                              <ResultSubTitleDetailing>{varCapitalMesAtual}</ResultSubTitleDetailing>
                              <SubTitleDetailing>Valor Atual</SubTitleDetailing>
                              <ResultSubTitleDetailing>{varCapitalSocialAtual}</ResultSubTitleDetailing>
                          </InfoCurrentCapital>
                        </DetailingCurrentCapitalRight> 
                      </RowCurrentCapital>

                    </ScrollView>

                  </>          
                
              }       
            
          </Container>            

      </Background>
  );
}