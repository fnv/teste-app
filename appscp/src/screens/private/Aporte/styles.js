import { Platform } from 'react-native';
import styled from 'styled-components/native';


export const Container = styled.KeyboardAvoidingView.attrs({
    enabled: Platform.OS === 'ios',
    behavior: 'padding',
})`
    flex: 1;
`;

export const Title = styled.Text`
   font-size: 20px;
   color: #000;
   font-weight: bold;
   align-self: center;
   margin-top: 10px;
   margin-bottom: 10px;
`;

export const List = styled.FlatList.attrs({
    showsVerticalScrollIndicator: false,
    contentContainerStyle: { padding: 30 },
})``;


export const ButtonAttachment = styled.Button`
width: 150,
height: 50,
borderRadius: 3,
backgroundColor: '#7159c1',
justifyContent: 'center',
alignItems: 'center',
marginTop: 20
`;

export const TextButtonAttachment = styled.Text` 
    color: #fff;
    font-weight: bold;
    font-size: 16px;
`;
