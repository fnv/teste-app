import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
//import { View, Text, Image, Alert } from 'react-native';

import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native'; 

import Background from '~/components/BackgroundLogged';
import Header from '~/components/Header';

import { Container, Title, Services, Avatar, Name, ServicesList, ServicesRow, ViewServices } from './styles2';

export default function Mensagem() {

  const navigation = useNavigation(); //faz a parte de navegação entre telas
  
  return (
      <Background>
          <Header />
          <Container>              
          <Title>Mensagem</Title>

          <ServicesRow>
            
            <Services>
              <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                <ViewServices>    
                  <Icon name="mail" size={30} color="#084296" />
                  <Name>Nova Mensagem</Name>
                </ViewServices>
              </TouchableOpacity>
            </Services>

            <Services>
              <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                <ViewServices>    
                  <Icon name="mail-open-sharp" size={30} color="#084296" />
                  <Name>Mensagens</Name>
                </ViewServices>
              </TouchableOpacity>
            </Services>

          </ServicesRow>

          </Container> 
      </Background>
  );
}

Mensagem.navigationOptons = {
  tabBarLabel: 'Mensagem',
  tabBarIcon: ({ tintColor }) => (
    <Icon name="settings" size={20} color={tintColor} />
  ),
};

