import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native'; 

import Background from '~/components/BackgroundLogged';
import Header from '~/components/Header';

import { Container, Title, Services, Avatar, Name, ServicesList, ServicesRow, ViewServices } from './styles';

export default function Resultados() {

  const navigation = useNavigation(); //faz a parte de navegação entre telas
  
  return (
      <Background>
          <Header />
          <Container>              
            <Title>Resultados</Title>

            <ScrollView>
              <ServicesRow>
                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="menu" size={30} color="#084296" />
                      <Name>Gráficos</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>

                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="ios-add-circle-sharp" size={30} color="#084296" />
                      <Name>Resultado</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>
              </ServicesRow>

              <ServicesRow>
                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="ios-remove-circle-sharp" size={30} color="#084296" />
                      <Name>Capital S</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>

                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="people" size={30} color="#084296" />
                      <Name>Receitas</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>
              </ServicesRow>

              <ServicesRow>
                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="ios-remove-circle-sharp" size={30} color="#084296" />
                      <Name>Despesas S</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>

                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="people" size={30} color="#084296" />
                      <Name>Impostos</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>
              </ServicesRow>

              <ServicesRow>
                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="ios-remove-circle-sharp" size={30} color="#084296" />
                      <Name>Lucro Líquido</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>

                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="people" size={30} color="#084296" />
                      <Name>Índice de Participação</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>
              </ServicesRow>

              <ServicesRow>
                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="ios-remove-circle-sharp" size={30} color="#084296" />
                      <Name>Distribuição</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>

                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Notificacao')} >
                    <ViewServices>  
                      <Icon name="people" size={30} color="#084296" />
                      <Name>Fundo de Reserva</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>
              </ServicesRow>
            </ScrollView>

          </Container> 
      </Background>
  );
}

Resultados.navigationOptons = {
  tabBarLabel: 'Resultados',
  tabBarIcon: ({ tintColor }) => (
    <Icon name="settings" size={20} color={tintColor} />
  ),
};

