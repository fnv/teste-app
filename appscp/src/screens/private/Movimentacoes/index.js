import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native'; 

import Background from '~/components/BackgroundLogged';
import Header from '~/components/Header';

import { Container, Title, Services, Avatar, Name, ServicesList, ServicesRow, ViewServices } from './styles2';

export default function Movimentacoes() {

  const navigation = useNavigation(); //faz a parte de navegação entre telas
  
  return (
      <Background>
          <Header />
          <Container>              
            <Title>Movimentações</Title>
            
            <ScrollView>
              <ServicesRow>
                
                <Services>      
                  <TouchableOpacity onPress={() => navigation.navigate('Extrato')} >
                    <ViewServices>            
                      <Icon name="menu" size={30} color="#084296" />                  
                      <Name>Extrato</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>                

                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Aporte')} >   
                    <ViewServices>
                      <Icon name="ios-add-circle-sharp" size={30} color="#084296" />
                      <Name>Aporte</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>
              </ServicesRow>

              <ServicesRow>
                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('Resgate')} >   
                    <ViewServices>
                      <Icon name="ios-remove-circle-sharp" size={30} color="#084296" />
                      <Name>Resgate</Name>
                    </ViewServices> 
                  </TouchableOpacity>
                </Services>

                <Services>
                  <TouchableOpacity onPress={() => navigation.navigate('MDs')} >    
                    <ViewServices>
                      <Icon name="people" size={30} color="#084296" />
                      <Name>MDs</Name>
                    </ViewServices>
                  </TouchableOpacity>
                </Services>
              </ServicesRow>
            </ScrollView>

          </Container> 
      </Background>
  );
}

Movimentacoes.navigationOptons = {
  tabBarLabel: 'Movimentações',
  tabBarIcon: ({ tintColor }) => (
    <Icon name="settings" size={20} color={tintColor} />
  ),
};

