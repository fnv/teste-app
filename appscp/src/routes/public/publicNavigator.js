import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Image, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

// Screens
import LogIn from '../../screens/public/LogIn';
import Register from '../../screens/public/Register';
import RegisterPF from '../../screens/public/RegisterPF';
import RegisterPJ from '../../screens/public/RegisterPJ';
import RegisterAddress from '../../screens/public/RegisterAddress';
import RegisterBank from '../../screens/public/RegisterBank';
import IForgotMyPassword from '../../screens/public/IForgotMyPassword';


const Stack = createStackNavigator();
 
function PublicNavigator({ navigation }) {
  return (
    <Stack.Navigator
        screenOptions={{ 
        headerShown: false
      }}
    >
      <Stack.Screen name="LogIn" component={LogIn} />
      <Stack.Screen name="Register" component={Register} 
      /* personalizando a statusbar com bt de voltar e nome da tela */
        options={{
          title: 'Cadastro',
          headerShown: true,
          headerLeft: () => (
            <TouchableOpacity 
            onPress={() => {
              navigation.navigate('LogIn');
            }}
            > 
              <Icon name="chevron-left" size={20} color="#000" />            
            </TouchableOpacity>
          ),
        }}
      />
      <Stack.Screen name="RegisterPF" component={RegisterPF} 
      /* personalizando a statusbar com bt de voltar e nome da tela */
      options={{
        title: 'Para Você',
        headerShown: true,
        headerLeft: () => (
          <TouchableOpacity 
          onPress={() => {
            navigation.navigate('Register');
          }}
          > 
            <Icon name="chevron-left" size={20} color="#000" />            
          </TouchableOpacity>
        ),
      }}     
      />
      <Stack.Screen name="RegisterPJ" component={RegisterPJ} 
      /* personalizando a statusbar com bt de voltar e nome da tela */
      options={{
        title: 'Para sua Empresa',
        headerShown: true,
        headerLeft: () => (
          <TouchableOpacity 
          onPress={() => {
            navigation.navigate('Register');
          }}
          > 
            <Icon name="chevron-left" size={20} color="#000" />            
          </TouchableOpacity>
        ),
      }}      
      />
      <Stack.Screen name="IForgotMyPassword" component={IForgotMyPassword} 
      /* personalizando a statusbar com bt de voltar e nome da tela */
      options={{
        title: 'Recuperar senha',
        headerShown: true,
        headerLeft: () => (
          <TouchableOpacity 
          onPress={() => {
            navigation.navigate('LogIn');
          }}
          > 
            <Icon name="chevron-left" size={20} color="#000" />            
          </TouchableOpacity>
        ),
      }} 
      />
      <Stack.Screen name="RegisterAddress" component={RegisterAddress} 
      /* personalizando a statusbar com bt de voltar e nome da tela */
      options={{
        title: 'Seu endereço',
        headerShown: true,
        headerLeft: () => (
          <TouchableOpacity 
          onPress={() => {
            navigation.navigate('RegisterPF');
          }}
          > 
            <Icon name="chevron-left" size={20} color="#000" />            
          </TouchableOpacity>
        ),
      }} 
      />
      <Stack.Screen name="RegisterBank" component={RegisterBank} 
      /* personalizando a statusbar com bt de voltar e nome da tela */
      options={{
        title: 'Dados bancários',
        headerShown: true,
        headerLeft: () => (
          <TouchableOpacity 
          onPress={() => {
            navigation.navigate('RegisterAddress');
          }}
          > 
            <Icon name="chevron-left" size={20} color="#000" />            
          </TouchableOpacity>
        ),
      }} 
      />
    </Stack.Navigator>
  );
}

export default PublicNavigator;