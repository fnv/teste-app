import * as React from 'react';
import { View, Text} from 'react-native';
//import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/Ionicons';
Icon.loadFont();

 
// Screens
import Home from '../../screens/private/Dashboard';


import Servicos from '../../screens/private/Servicos';
import ServicosItens from '../../screens/private/ServicosItens';
import Notificacao from '../../screens/private/Notificacao';

import Resultados from '../../screens/private/Resultados';
import Mensagem from '../../screens/private/Mensagem';

//movimentações
import Movimentacoes from '../../screens/private/Movimentacoes';
import Extrato from '../../screens/private/Extrato';
import Aporte from '../../screens/private/Aporte';
import Resgate from '../../screens/private/Resgate';
import MDs from '../../screens/private/Extrato';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

//sub menu home-----------------------------------------------------------------
function HomeTabs() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
     initialRouteName="Home"
    > 
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="ServicosItens" component={ServicosItens} />
      <Stack.Screen name="Notificacao" component={Notificacao} />
    </Stack.Navigator>
  );
}
//sub menu movimentações-----------------------------------------------------------------
function MovimentacoesTabs() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName="Movimentacoes"
    > 
      <Stack.Screen name="Movimentacoes" component={Movimentacoes} />
      <Stack.Screen name="Extrato" component={Extrato} />
      <Stack.Screen name="Aporte" component={Aporte} />
      <Stack.Screen name="Resgate" component={Resgate} />
      <Stack.Screen name="MDs" component={MDs} />
    </Stack.Navigator>
  );
}

//sub menu resultados-----------------------------------------------------------------
function ResultadosTabs() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
    
    > 
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="ServicosItens" component={ServicosItens} />
      <Stack.Screen name="Notificacao" component={Notificacao} />
      </Stack.Navigator>
  );
}

//sub menu mensagem-----------------------------------------------------------------
function MensagemTabs() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
    
    > 
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="ServicosItens" component={ServicosItens} />
      <Stack.Screen name="Notificacao" component={Notificacao} />
      </Stack.Navigator>
  );
}




function PrivateTabNavigator() {
  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName; 

      console.log('+++++++++++++++++++++++++++++++++');
        console.log(route);

        if (route.name === 'Home') {
          iconName = focused ? 'md-home' : 'home-outline';
        } else if (route.name === 'Movimentações') {
          iconName = focused ? 'document-text' : 'document-text-outline';
        } else if (route.name === 'Resultados') {
          iconName = focused ? 'bar-chart' : 'bar-chart-outline';
        } else if (route.name === 'Mensagem') {
          iconName = focused ? 'mail-sharp' : 'mail-outline';
        }
 
        // You can return any component that you like here!
        return <Icon name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#fff',
      inactiveTintColor: '#fff',
      style: {
        backgroundColor: '#9f1717',
      },
    }}             
    >  
      <Tab.Screen name="Home" component={HomeTabs} />
      <Tab.Screen name="Movimentações" component={MovimentacoesTabs} />
      <Tab.Screen name="Resultados" component={Resultados} />
      <Tab.Screen name="Mensagem" component={Mensagem} />
    </Tab.Navigator>
  );
}

export default PrivateTabNavigator;